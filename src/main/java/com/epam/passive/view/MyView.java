package com.epam.passive.view;

import com.epam.passive.controller.*;
import org.apache.logging.log4j.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger logger1 = LogManager.getLogger(MyView.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - array logic task");
        menu.put("2", "  2 - array logic task");
        menu.put("3", "  3 - door Game");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton1() {
        controller.unionArray();
    }

    private void pressButton2() {

        controller.containsOneMasive();
    }

    private void pressButton3() {
        controller.doorGame();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
